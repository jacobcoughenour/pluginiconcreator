precision mediump float;

varying vec2 vUvs;

#define PI 3.14159265359

uniform float colors[12];
uniform int len;
uniform float angle;
uniform float scale;

void main() {

	// 2 color gradient
	if (len == 2) {

		float a = angle * PI;
		vec2 point = (vUvs - vec2(.5)) * mat2(cos(a), -sin(a), sin(a), cos(a));

		if (scale == 0.) {
			gl_FragColor = (point.y < 0.) ?
				vec4(colors[0], colors[1], colors[2], 1.) :
				vec4(colors[3], colors[4], colors[5], 1.);
		} else
			gl_FragColor = vec4(
				mix(
					vec3(colors[0], colors[1], colors[2]),
					vec3(colors[3], colors[4], colors[5]),
					(point.y + scale / 2.) / scale
				),
				1.
			);

		return;
	}

	// 3 color gradient
	if (len == 3) {

		vec2 center = vec2(.5);

		if (scale == .0) {

			float ap = -angle * PI;
			vec2 pos = (center - vUvs) * mat2(cos(ap), -sin(ap), sin(ap), cos(ap));
			float t = atan(pos.x, pos.y);

			if (t <= -PI / 3.)
				gl_FragColor = vec4(vec3(colors[0], colors[1], colors[2]), 1.);
			else if (t <= PI / 3.)
				gl_FragColor = vec4(vec3(colors[3], colors[4], colors[5]), 1.);
			else
				gl_FragColor = vec4(vec3(colors[6], colors[7], colors[8]), 1.);

			return;
		}

		// three rotation angles to form a triangle
		vec3 a = vec3(angle + .6666, angle, angle - .6666) * PI;

		// point to rotate
		vec2 point = vec2(scale);

		// distances between vUvs and three points of the triangle
		vec3 sample = normalize(
			vec3(
				distance(vUvs, point * mat2(cos(a.r), -sin(a.r), sin(a.r), cos(a.r)) + center),
				distance(vUvs, point * mat2(cos(a.g), -sin(a.g), sin(a.g), cos(a.g)) + center),
				distance(vUvs, point * mat2(cos(a.b), -sin(a.b), sin(a.b), cos(a.b)) + center)
			)
		);

 		// so we don't have any zeros
		sample += 0.001;

		// weird rational blending
		sample *= vec3(sample.r / sample.g, sample.g / sample.b, sample.b / sample.r) * 0.5;
		sample = normalize(sample);
		sample *= sample;


		gl_FragColor = vec4(
			(vec3(colors[0], colors[1], colors[2]) * sample.r) +
			(vec3(colors[3], colors[4], colors[5]) * sample.g) +
			(vec3(colors[6], colors[7], colors[8]) * sample.b),
			1.
		);

		return;
	}

	// 4 color gradient
	if (len == 4) {

		float a = angle * PI;
		vec2 point = (vUvs - vec2(.5)) * mat2(cos(a), -sin(a), sin(a), cos(a));

		if (scale == 0.) {
			gl_FragColor = (point.y < 0.) ?
				((point.x < 0.) ?
					vec4(colors[0], colors[1], colors[2], 1.) :
					vec4(colors[3], colors[4], colors[5], 1.)
				) :
				((point.x < 0.) ?
					vec4(colors[6], colors[7], colors[8], 1.) :
					vec4(colors[9], colors[10], colors[11], 1.)
				);

		} else {
			float s = scale * .5 + .5;
			gl_FragColor = vec4(
				mix(
					mix(
						vec3(colors[0], colors[1], colors[2]),
						vec3(colors[3], colors[4], colors[5]),
						(point.y + s / 2.) / s
					),
					mix(
						vec3(colors[6], colors[7], colors[8]),
						vec3(colors[9], colors[10], colors[11]),
						(point.y + s / 2.) / s
					),
					(point.x + s / 2.) / s
				),
				1.
			);
		}

		return;
	}

	gl_FragColor = vec4(colors[0], colors[1], colors[2], 1.);
}