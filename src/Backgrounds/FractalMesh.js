import { BaseBackgroundShader, DisplayName } from './BaseBackgroundShader';
import * as PC from './PropertyControllers';
import * as PIXI from 'pixi.js';
import fragment from './fractalmesh.frag';
import { iconSize } from '../app';

@DisplayName("Fractal Mesh")
export default class FractalMesh extends BaseBackgroundShader {

	static defaults = {
		c: [0xE65100, 0xBF360C, 0xb71c1c],
		sh: 0x263238,
		sa: 0.5,
		sc: 8,
		t: 0,
		se: 0
	};

	constructor(data = {}) {

		data = Object.assign(FractalMesh.defaults, data);

		let uniforms = {
			colors: Float32Array.from(data.c.reduce((ac, e) => ac.concat(PIXI.utils.hex2rgb(e)), [])),
			shadow: Float32Array.from(PIXI.utils.hex2rgb(data.sh)),
			shadeamount: data.sa,
			scale: data.sc,
			resolution: Float32Array.from([iconSize, iconSize]),
			time: data.t,
			seed: data.se
		};

		let controllers = {
			Colors: new PC.ColorArray("colors", "", data.c, 3, 3, 3),
			Shadows: new PC.Color("shadow", data.sh),
			Shading: new PC.Range("shadeamount", data.sa, 0, 1, 0.05),
			Scale: new PC.Range("scale", data.sc, 4, 16, 1),
			Move: new PC.Range("time", data.t, 0, 2, 0.05),
			Seed: new PC.Button("New Seed", (e) => {
				e.shader.uniforms.seed = Math.random() * 100000;
			})
		};

		super(fragment, uniforms, controllers);

		if (data.se === 0)
			this.uniforms.seed = Math.random() * 100000;
	}

	toData() {

		let hexcolors = [];

		for (let i = 0; i < this.uniforms.colors.length / 3; i++) {
			hexcolors.push(
				PIXI.utils.rgb2hex([
					this.uniforms.colors[i * 3],
					this.uniforms.colors[i * 3 + 1],
					this.uniforms.colors[i * 3 + 2]
				])
			);
		}

		return {
			c: hexcolors,
			sh: PIXI.utils.rgb2hex(this.uniforms.shadow),
			sa: this.uniforms.shadeamount,
			sc: this.uniforms.scale,
			t: this.uniforms.time,
			se: this.uniforms.seed
		};
	}
}
