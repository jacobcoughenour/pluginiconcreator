import { BaseBackgroundShader, DisplayName } from './BaseBackgroundShader';
import * as PC from './PropertyControllers';
import * as PIXI from 'pixi.js';
import fragment from './gradient.frag';

@DisplayName("Gradient")
export default class Gradient extends BaseBackgroundShader {

	static defaults = {
		c: [0x00C853, 0x00B8D4, 0x0091EA, 0x3D5AFE],
		a: 0.5,
		s: 1.4
	};

	constructor(data = {}) {

		data = Object.assign(Gradient.defaults, data);

		let uniforms = {
			colors: Float32Array.from(data.c.reduce((ac, e) => ac.concat(PIXI.utils.hex2rgb(e)), [])),
			len: data.c.length,
			angle: data.a,
			scale: data.s
		};

		let properties = {
			Colors: new PC.ColorArray("colors", "len", data.c, 3, 2, 4),
			Angle: new PC.Range("angle", data.a, 0, 2, 0.05),
			Distance: new PC.Range("scale", data.s, 0, 3, 0.2)
		};

		super(fragment, uniforms, properties);
	}

	toData() {

		let hexcolors = [];

		for (let i = 0; i < this.uniforms.len; i++) {
			hexcolors.push(
				PIXI.utils.rgb2hex([
					this.uniforms.colors[i * 3],
					this.uniforms.colors[i * 3 + 1],
					this.uniforms.colors[i * 3 + 2]
				])
			);
		}

		return {
			c: hexcolors,
			a: this.uniforms.angle,
			s: this.uniforms.scale
		};
	}
}
