precision mediump float;

varying vec2 vUvs;

#define PI 3.14159265359

uniform vec2 resolution;
uniform float time;
uniform float seed;

uniform float colors[9];
uniform vec3 shadow;
uniform float shadeamount;
uniform float scale;

vec2 random2(vec2 p) {
	return fract(sin(vec2(dot(p,vec2(127.1,311.7)),dot(p,vec2(269.5,183.3))))*seed);
}

void main() {
	vec2 st = gl_FragCoord.xy/resolution.xy;
	st.x *= resolution.x/resolution.y;

	// Scale
	st -= .5;
	st *= scale;

	// Tile the space
	vec2 i_st = floor(st);
	vec2 f_st = fract(st);

	float m_dist = 1.;  // minimun distance
	vec2 m_point;       // minimum position

	for (int y= -1; y <= 1; y++) {
		for (int x= -1; x <= 1; x++) {
			// Neighbor place in the grid
			vec2 neighbor = vec2(float(x),float(y));

			// Random position from current + neighbor place in the grid
			vec2 point = random2(i_st + neighbor);

			// Animate the point
			point = 0.5 + 0.5*sin((time * PI) + 6.2831*point);

			// Vector between the pixel and the point
			vec2 diff = neighbor + point - f_st;

			// Distance to the point
			float dist = length(diff) * 0.6;

			if (dist < m_dist) {
				m_dist = dist;
				m_point = point;
			}
		}
	}

	// m_point /= 2.;
	// m_point += .5;

	// fake ambient occlusion
	float shade = max(pow(m_dist, pow(1. - shadeamount / 2., 2.) * 4.), 0.) * m_point.y * shadeamount;

	// Draw cell center
	//shade += 1.-step(.02, m_dist);

	// Draw grid
	//shade += step(.98, f_st.x) + step(.98, f_st.y);

	// Show isolines
	//shade -= step(.7,abs(sin(27.0*m_dist)))*.1;

	gl_FragColor = vec4(
			mix(
				mix(
					vec3(colors[6], colors[7], colors[8]),
					mix(
						vec3(colors[0], colors[1], colors[2]),
						vec3(colors[3], colors[4], colors[5]),
				 		m_point.x
					),
					m_point.y
				),
				shadow,
				shade
			),
		1.0);
}
