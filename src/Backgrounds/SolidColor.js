import { BaseBackgroundShader, DisplayName } from './BaseBackgroundShader';
import * as PC from './PropertyControllers';
import * as PIXI from 'pixi.js';
import fragment from './solidcolor.frag';


@DisplayName("Solid Color")
export default class SolidColor extends BaseBackgroundShader {

	static defaults = {
		c: 0x007BFF
	};

	constructor(data = {}) {

		data = Object.assign(SolidColor.defaults, data);

		let uniforms = {
			color: Float32Array.from(PIXI.utils.hex2rgb(data.c))
		};

		let controllers = {
			Color: new PC.Color("color", data.c)
		};

		super(fragment, uniforms, controllers);
	}

	toData() {
		return {
			c: PIXI.utils.rgb2hex(this.uniforms.color)
		};
	}
}
