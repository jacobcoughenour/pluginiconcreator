import PropertyController from "./PropertyController";
import $ from 'jquery';

/**
 * Property Controller for calling a function with a button.
 */
export default class Button extends PropertyController {

	/**
	 * Button Property Controller constructor
	 * @param {string} buttonLabel button label
	 * @param {string} value initial color (ex: 0xFFFFFF)
	 */
	constructor(buttonLabel = "button", bindCallback = () => {}) {

		// create PropertyController with our custom input element
		super($(`<button class="btn">${buttonLabel}</button>`), "click", bindCallback, false);
	}
}
