import $ from 'jquery';

/**
 * Creates and handles elements for controlling a shader property
 */
export default class PropertyController {

	/**
	 * PropertyController constructor
	 * @param {JQuery<HTMLElement>} inputElement
	 * @param {string} bindName event name for input
	 * @param {Function} bindCallback input event callback
	 * Ex: (e) => { e.shader.uniform.a = e.event.target.value; }
	 */
	constructor(inputElement, bindName = "", bindCallback = null, hasLabel = true) {
		this.inputElement = inputElement.addClass("property-input");
		this.bindName = bindName;
		this.bindCallback = bindCallback;
		this.hasLabel = hasLabel;
	}

	/**
	 * Attaches this property controller to shader and creates all the DOM elements and input handlers
	 * @param {PIXI.Shader} shader shader to attach this controller to
	 * @param {string} label property label
	 * @returns property element as jQuery
	 */
	load(shader, label) {
		// reference to the shader this property belongs to.
		// use this as a way to apply changes to the shader from event listeners.
		this.shader = shader;

		// bind input event listener(s)
		this.bind();

		// create property element
		let e = this.hasLabel ?
			$(`<div class="property"><label>${label}</label></div>`) :
			$(`<div class="property"></div>`);

		// append the inputElement to it
		e.append(this.inputElement);

		// return complete property element
		return e;
	}

	/**
	 * Bind input event handler to this.inputElement.
	 * override for custom event binding
	 */
	bind() {
		this.inputElement.bind(this.bindName, e => {
			this.bindCallback({ event: e, shader: this.shader });
			this.shader.redraw();
		});
	}
}
