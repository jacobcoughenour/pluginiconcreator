import PropertyController from "./PropertyController";
import $ from 'jquery';

/**
 * Property Controller for an array of colors.
 */
export default class ColorArray extends PropertyController {

	/**
	 * Color Property Controller constructor
	 * @param {string} colorsUniformName float[] uniform of rgb values you want this controller to modify
	 * @param {string} lengthUniformName int uniform for the color length
	 * @param {number[]} values initial colors (ex: [0xFFFFFF])
	 * @param {number} minLength minimum length of color array
	 * @param {number} maxLength maximum length of color array
	 */
	constructor(colorsUniformName = "colors", lengthUniformName = "length", values = [0xFFFFFF], initLength = 0, minLength = 0, maxLength = 0) {

		// create PropertyController with our custom element
		super($('<span class="d-inline-grid"></span>'));

		this.colorlist = $(`<span class="d-inline-grid list-values${minLength !== maxLength ? "" : " no-corner"}"></span>`);
		this.inputElement.append(this.colorlist);

		let buttons = $('<span class="list-add-remove"></span>')

		if (minLength !== maxLength) {
			this.addButtonElement = $(`<button class="btn btn-sm"><i class="fas fa-plus"></i></button>`);
			this.removeButtonElement = $(`<button class="btn btn-sm"><i class="fas fa-minus"></i></button>`);
			buttons.append(this.addButtonElement);
			buttons.append(this.removeButtonElement);
		}

		this.inputElement.append(buttons);

		this.colorsUniformName = colorsUniformName;
		this.lengthUniformName = lengthUniformName;
		this.currentLength = initLength;
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.currentValues = Array.from({length: maxLength}, (e,i) => i < values.length ? values[i] : 0x000000);

		this.updateButtons();
	}

	addColor() {

		if (this.maxLength <= this.currentLength)
			return;

		this.currentLength++;

		this.updateColors();
		this.updateButtons();
	}

	removeColor() {

		if (this.minLength >= this.currentLength)
			return;

		this.currentLength--;

		this.updateColors();
		this.updateButtons();
	}

	// update + and - button states
	updateButtons() {
		this.addButtonElement?.prop("disabled", this.maxLength <= this.currentLength);
		this.removeButtonElement?.prop("disabled", this.minLength >= this.currentLength);
	}

	updateColors() {

		// clear list
		this.colorlist.empty();

		for (let i = 0; i < this.currentLength; i++) {

			// hex color converted to string
			let color = this.currentValues[i].toString(16).padStart(6, 0);

			// color button element
			let e = $(`<button class="btn color-input" style="background:#${color};"></button>`);

			// attach colpick
			e.colpick({
				submit: false,
				color: color,
				onChange: (hsb, hex, rgb) => {
					this.shader.uniforms[this.colorsUniformName][i * 3] = rgb.r / 255;
					this.shader.uniforms[this.colorsUniformName][i * 3 + 1] = rgb.g / 255;
					this.shader.uniforms[this.colorsUniformName][i * 3 + 2] = rgb.b / 255;
					this.currentValues[i] = parseInt(hex, 16);

					e.css('background', `#${hex}`);
					this.shader.redraw();
				}
			});

			// append button to list
			this.colorlist.append(e);

			// update the colors uniform
			this.shader.uniforms[this.colorsUniformName][i * 3] = parseInt(color.slice(0,2), 16) / 255;
			this.shader.uniforms[this.colorsUniformName][i * 3 + 1] = parseInt(color.slice(2, 4), 16) / 255;
			this.shader.uniforms[this.colorsUniformName][i * 3 + 2] = parseInt(color.slice(4, 6), 16) / 255;
		}

		// update the length uniform
		if (this.lengthUniformName.length > 0)
			this.shader.uniforms[this.lengthUniformName] = this.currentLength;

		// redraw the shader
		this.shader.redraw();
	}

	/**
	 * bind override to handle colpick input
	 */
	bind() {

		this.updateColors();

		this.addButtonElement?.bind("click", () => { this.addColor(); });
		this.removeButtonElement?.bind("click", () => { this.removeColor(); });
	}

}
