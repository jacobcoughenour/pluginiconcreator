import PropertyController from "./PropertyController";
import $ from 'jquery';

/**
 * Property Controller for int or float values.
 */
export default class Range extends PropertyController {

	/**
	 * Range Property Controller constructor
	 * @param {string} uniformName float or int uniform to control
	 * @param {number} value initial value
	 * @param {number} min minimum value
	 * @param {number} max maximum value
	 * @param {number} step slider granularity
	 */
	constructor(uniformName = "angle", value = 0, min = 0, max = 10, step = 1) {

		// create PropertyController with our custom input element
		super(
			$(`<input type="range" value="${value}" min="${min}" max="${max}" step="${step}">`),
			"input",
			e => {
				e.shader.uniforms[uniformName] = e.event.target.value;
			}
		);

		this.uniformName = uniformName;
	}
}
