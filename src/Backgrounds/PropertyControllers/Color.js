import PropertyController from "./PropertyController";
import $ from 'jquery';

/**
 * Property Controller for Color values.
 */
export default class Color extends PropertyController {

	/**
	 * Color Property Controller constructor
	 * @param {string} uniformName vec3 uniform you want this controller to modify
	 * @param {number} value initial color (ex: 0xFFFFFF)
	 */
	constructor(uniformName = "color", value = 0xFFFFFF) {

		// create PropertyController with our custom input element
		super($(`<button class="btn color-input" style="background:#${value.toString(16).padStart(6, 0)};"></button>`));

		this.value = value;
		this.uniformName = uniformName;
	}

	/**
	 * bind override to handle colpick input
	 */
	bind() {

		// add colpick input
		this.inputElement.colpick({
			submit: false,
			color: this.value.toString(16).padStart(6, 0),
			onChange: (hsb, hex, rgb) => {
				this.shader.uniforms[this.uniformName] = Float32Array.from([rgb.r / 255, rgb.g / 255, rgb.b / 255]);
				this.inputElement.css('background', `#${hex}`);
				this.shader.redraw();
			}
		});
	}
}
