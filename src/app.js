import $ from 'jquery';
import * as PIXI from 'pixi.js';
import swal from 'sweetalert2';
import Dropzone from 'dropzone';
import yaml from 'js-yaml';
import 'jquery-colpick';
import 'jquery-colpick/css/colpick.css';
import './typingmode';

import { DropShadowFilter } from './Filters';
import * as Backgrounds from './Backgrounds';

import template from './template.html';
import './app.css';

/** Global icon resolution used for scaling */
export const iconSize = 320;

/** zoom amount */
export var curZoom = 3;
export const zoomSteps = [0.1, 0.2, 0.33, 0.5, 1];

/** Current PIXI.Application instance */
export var app;

/** @type {PIXI.Text} */
export var text;
/** @type {PIXI.Text} */
export var textshadow;
/** @type {DropShadowFilter} */
export var dropshadow;
/** @type {BackgroundShader} */
export var bgshader;
/** @type {PIXI.Mesh} */
export var bgmesh;

/** @type {Selectize.IApi<any, any>} */
export var iconselect;
/** @type {JQuery<HTMLElement>} */
export var iconcolorinput;
/** @type {JQuery<HTMLElement>} */
export var shadowcolorinput;
/** @type {JQuery<HTMLElement>} */
export var bgtypeselect;

// eslint-disable-next-line
console.log(`Icon Creator -${(typeof __netlify__ !== 'undefined' ? ' ' + __netlify__ : '')} hash:${__webpack_hash__} builtat:${__built_at__}`);

export var currentProperties = {};
export const defaultProperties = {
	fg: {
		glyph: 'f1e2',
		color: 0xFFFFFF,
		scale: 0,
		offset: 0,
		shadow: {
			color: 0x333333,
			alpha: 0.7,
			blur: 0
		}
	},
	bg: {
		type: 'Gradient',
		data: {}
	}
};

// load properties
export function importProperties(props) {

	props.fg = Object.assign(props.fg, defaultProperties.fg);

	if (!props.bg || !props.bg.type || !Backgrounds.hasOwnProperty(props.bg.type)) {
		props.bg = defaultProperties.bg;
	}

	bgshader.destroy();
	bgshader = new Backgrounds[props.bg.type](props.bg.data);
	bgshader.loadControllers($('.iconcreator .bg-properties'));
	currentProperties.bg = { type: props.bg.type };
	bgmesh.shader = bgshader;

	bgtypeselect.val(props.bg.type);

	currentProperties.fg = {
		glyph: props.fg.glyph,
		color: props.fg.color,
		scale: parseFloat(props.fg.scale),
		offset: parseFloat(props.fg.offset),
		shadow: Object.assign(defaultProperties.fg.shadow, props.fg.shadow)
	};

	text.text = textshadow.text = String.fromCharCode(parseInt(props.fg.glyph, 16));
	iconselect?.setValue(props.fg.glyph, true);

	iconcolorinput.colpickSetColor(currentProperties.fg.color.toString(16).padStart(6, 0), true);

	$('.iconcreator input[name="icon-scale"]').val(props.fg.scale);
	setIconSize(props.fg.scale);
	$('.iconcreator input[name="icon-offset"]').val(props.fg.offset);
	setIconOffset(props.fg.offset);

	shadowcolorinput.colpickSetColor(currentProperties.fg.shadow.color.toString(16).padStart(6, 0), true);

	$('.iconcreator input[name="shadow-alpha"]').val(props.fg.shadow.alpha);
	dropshadow.alpha = props.fg.shadow.alpha;

	$('.iconcreator input[name="shadow-blur"]').val(props.fg.shadow.blur);
	setShadowBlur(props.fg.shadow.blur);

	app.ticker.start();
}

// save properties and download them as a json file
export function saveProperties() {

	currentProperties.bg.data = bgshader.toData();

	// download the json file

	if (window.navigator.msSaveBlob) { // IE/Edge

		window.navigator.msSaveOrOpenBlob(new Blob([JSON.stringify(currentProperties)], { type: "application/json" }), "iconprops.json");

	} else {

		let a = document.createElement("a");
		let json = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(currentProperties));
		a.href = json;
		a.download = "iconprops.json";
		document.body.appendChild(a) // required for firefox
		a.click();
		a.remove();
	}
}

// loads the icon glyph selector
export function loadIconSelector() {

	// get icon list
	$.get('https://assets.umod.org/yml/icons.yml', data => {

		// convert to json
		let json = yaml.load(data);

		// convert json to an array of objects with only relevent info
		let icons = Object.keys(json).reduce((list, key) => {

			// skip "brand" icons
			if (json[key].styles.includes("brands"))
				return list;

			list.push({
				key: key,
				unicode: json[key].unicode,
				label: json[key].label,
				tags: json[key].search.terms
			});

			return list;
		}, []);

		// create selectize dropdown
		let $select = $('.fg-properties select[name="icon"]').selectize({
			plugins: ['typing_mode'],
			valueField: 'unicode',
			labelField: 'label',
			searchField: ['label', 'key', 'tags'],
			create: false,
			options: icons,
			items: [defaultProperties.fg.glyph],
			usePlaceholder: true,
			render: {
				option: (item, escape) =>
					`<div class="item">
						<span class="title">
							<i class="icon fa fa-${escape(item.key)}"></i>${escape(item.label)}
						</span>
					</div>`,
				item: (item, escape) =>
					`<div class="item selected">
						<span class="title">
							<i class="icon fa fa-${escape(item.key)}"></i>${escape(item.label)}
						</span>
					</div>`
			},
			onChange: (value) => {
				text.text = textshadow.text = String.fromCharCode(parseInt(value, 16));
				currentProperties.fg.glyph = value;
				app.ticker.start();
			}
		});

		iconselect = $select[0].selectize;
	});

}

function setIconSize(value) {
	text.style.fontSize = textshadow.style.fontSize = (iconSize / 2) + (value * (iconSize * 0.15));
	currentProperties.fg.scale = parseFloat(value);
}

function setIconOffset(value) {
	text.position.set(iconSize / 2, (iconSize / 2) + (value * (iconSize * 0.1)));
	textshadow.position.set(iconSize / 2, (iconSize / 2) + (value * (iconSize * 0.1)));
	currentProperties.fg.offset = parseFloat(value);
}

function setShadowBlur(value) {
	dropshadow.blur = (iconSize / 32) + (value * (iconSize / 32));
	currentProperties.fg.shadow.blur = parseFloat(value);
}

function setZoom(value) {

	if (value < 0 || value > zoomSteps.length - 1)
		return;

	curZoom = value;

	let px = iconSize * zoomSteps[curZoom];

	$('.iconcreator .canvas-wrapper canvas#pixi').width(px).height(px);
	$('.iconcreator .canvas-wrapper').height(px);
	$('.iconcreator .canvas-wrapper #zoom-amount').html(zoomSteps[curZoom] * 100 + '%');

	$('.iconcreator #zoom-in').prop('disabled', curZoom === zoomSteps.length - 1);
	$('.iconcreator #zoom-out').prop('disabled', curZoom === 0);
}

window.openIconCreator = () => {

	swal({
		title: 'Icon Creator',
		html: template,
		showCloseButton: true,
		showCancelButton: true,
		focusConfirm: false,
		confirmButtonText: '<i class="fa fa-check"></i> Submit',
		confirmButtonAriaLabel: 'Save new icon',
		preConfirm: () => {
			app.renderer.extract.canvas().toBlob((blob) => {

				let dz = Dropzone.forElement('#icon-dropzone');
				blob.name = 'icon.png';
				dz.addFile(blob);

				// unload pixi instance
				window.destroypixi();

			}, 'image/png');
		}
	});

	PIXI.settings.PRECISION_FRAGMENT = PIXI.PRECISION.HIGH;

	// Create a Pixi App
	app = new PIXI.Application({
		width: iconSize,
		height: iconSize,
		antialias: true,
		transparent: false,
		preserveDrawingBuffer: true
	});
	app.view.id = 'pixi';

	// append canvas to DOM
	$('.iconcreator .canvas-wrapper').append(app.view);
	app.renderer.autoDensity = true;

	setZoom(curZoom);

	// create background
	bgshader = new Backgrounds.SolidColor();

	// create a quad
	let geometry = new PIXI.Geometry()
		.addAttribute('aVertexPosition', [0, 0, 1, 0, 1, 1, 0, 1], 2)
		.addAttribute('aUvs', [0, 0, 1, 0, 1, 1, 0, 1], 2)
		.addIndex([0, 1, 2, 0, 2, 3]);

	bgmesh = new PIXI.Mesh(geometry, bgshader);
	bgmesh.scale.set(iconSize);
	app.stage.addChild(bgmesh);

	// create icon

	let style = new PIXI.TextStyle({
		fontFamily: 'Font Awesome 5 Free',
		fontSize: iconSize / 2,
		fontWeight: 900,
		fill: '#ffffff',
		strokeThickness: 0,
		wordWrap: false,
		align: "center",
		padding: 10
	});

	text = new PIXI.Text('', style);
	text.anchor.set(0.5, 0.465);
	text.position.set(iconSize / 2);

	textshadow = new PIXI.Text('', style);
	textshadow.anchor.set(0.5, 0.465);
	textshadow.position.set(iconSize / 2);

	dropshadow = new DropShadowFilter({
		rotation: 0,
		distance: 0,
		color: 0x455A64,
		alpha: 1,
		shadowOnly: false,
		blur: iconSize / 32,
		quality: 8,
	});

	textshadow.filters = [dropshadow];

	app.stage.addChild(textshadow);
	app.stage.addChild(text);

	app.ticker.add(() => {
		// stop render loop after one frame
		app.ticker.stop();
	});


	// bind input event listeners


	$('.iconcreator #zoom-out').click(() => {
		setZoom(curZoom - 1);
	});

	$('.iconcreator #zoom-in').click(() => {
		setZoom(curZoom + 1);
	});

	$('.iconcreator #import-props').click(() => {

		var input = $(document.createElement("input"));
		input.attr("type", "file");
		input.attr("accept", "application/JSON");

		input.bind('change', (e) => {

			let reader = new FileReader();
			reader.onload = (read) => {
				importProperties(JSON.parse(read.target.result));
			};
			reader.readAsText(e.target.files[0]);
		});

		input.trigger("click");
		return false;
	});

	$('.iconcreator #export-props').click(() => {
		saveProperties();
	});


	// foreground properties

	loadIconSelector();

	iconcolorinput = $('.iconcreator button#icon-color');
	iconcolorinput.colpick({
		submit: false,
		color: 'FFFFFF',
		onChange: (hsb, hex) => {
			iconcolorinput.css('background', `#${hex}`);
			text.style.fill = `#${hex}`;
			currentProperties.fg.color = parseInt(hex, 16);
			app.ticker.start();
		}
	});
	iconcolorinput.css('background', '#FFFFFF');

	$('.iconcreator input[name="icon-scale"]').bind('input', e => {
		setIconSize(e.target.value);
		app.ticker.start();
	});

	$('.iconcreator input[name="icon-offset"]').bind('input', e => {
		setIconOffset(e.target.value);
		app.ticker.start();
	});

	shadowcolorinput = $('.iconcreator button#shadow-color');
	shadowcolorinput.colpick({
		submit: false,
		color: '000000',
		onChange: (hsb, hex) => {
			shadowcolorinput.css('background', `#${hex}`);
			dropshadow.color = parseInt(hex, 16);
			currentProperties.fg.shadow.color = parseInt(hex, 16);
			app.ticker.start();
		}
	});
	shadowcolorinput.css('background', '#FFFFFF');

	$('.iconcreator input[name="shadow-alpha"]').bind('input', e => {
		dropshadow.alpha = e.target.value;
		currentProperties.fg.shadow.alpha = parseFloat(e.target.value);
		app.ticker.start();
	});

	$('.iconcreator input[name="shadow-blur"]').bind('input', e => {
		setShadowBlur(e.target.value);
		app.ticker.start();
	});


	bgtypeselect = $('.iconcreator select[name="bgtype"]');

	// populate the bgtype select with BackgroundShaders from Backgrounds
	Object.keys(Backgrounds).forEach(bg => {
		bgtypeselect.append($(`<option value="${bg}">${Backgrounds[bg].displayName}</option>`));
	});

	// bind bgtype change handler
	bgtypeselect.bind('change', e => {

		bgshader.destroy();

		// change the bgshader to the selected one
		bgshader = new Backgrounds[e.target.value]();
		bgshader.loadControllers($('.iconcreator .bg-properties'));

		currentProperties.bg.type = e.target.value;

		bgmesh.shader = bgshader;

		app.ticker.start();
	});

	window.destroypixi = () => {
		app.destroy();
	};


	importProperties(defaultProperties);

};
