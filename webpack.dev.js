const path = require('path');
const webpack = require('webpack');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

module.exports = env => {
	return {
		mode: 'development',
		entry: {
			app: './src/app.js'
		},
		output: {
			filename: 'iconcreator.[name].js',
			path: path.resolve(__dirname, 'dist')
		},
		module: {
			rules: [
				{ test: /\.js$/, exclude: /node_modules/, loader: ['babel-loader', 'eslint-loader'] },
				{
					test: /\.css$/, use: [
						{ loader: 'style-loader' },
						{ loader: 'css-loader', options: { importLoaders: 1 } },
						{ loader: 'postcss-loader' }
					]
				},
				{ test: /\.html$/, loader: 'html-loader' },
				{ test: /\.frag$/, loader: 'raw-loader' },
				{ test: /\.vert$/, loader: 'raw-loader' }
			]
		},
		plugins: [
			new webpack.DefinePlugin({
				DEBUG: env === 'debug',
				__built_at__: JSON.stringify(new Date().toString())
			}),
			new webpack.ExtendedAPIPlugin(),
			new FriendlyErrorsWebpackPlugin({
				compilationSuccessInfo: {
					messages: env === 'debug' ? [
							'Global variable \'DEBUG\' set to \'true\'',
							'Project is running at http://localhost:9000'
						] : ['Project is running at http://localhost:9000']
				}
			})
		],
		devServer: {
			contentBase: path.join(__dirname, 'dist'),
			port: 9000,
			quiet: true,
			inline: false
		},
		externals: [
			{ "pixi.js": "PIXI" },
			{ "sweetalert2": "swal" },
			{ "jquery": "jQuery" },
			{ "selectize": "Selectize" },
			{ "dropzone" : "Dropzone" }
		],
		stats: {
			colors: true
		}
	};
};
